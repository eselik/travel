var app = new Vue({
    el: '#app',
    delimiters: ["[[","]]"],
    data: {
        message: 'Hello Vue!',
        pickup_point: "",
        dropoff_point: "",
        points: [
            {"text": "Point 1", "value": "1"},
            {"text": "Point 2", "value": "2"},
            {"text": "Point 3", "value": "3"},
            {"text": "Point 4", "value": "4"},
        ],
        modes_of_transport: [
            {"text": "Car", "value": "1"},
            {"text": "SUV", "value": "2"},
        ],
        options: [
            {},
            {}
        ]
    },
    methods: {
        displayOptions: function() {
            
        }
    }
})