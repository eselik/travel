var app = new Vue({
    el: '#app',
    delimiters: ["[[","]]"],
    data: {
        message: 'Hello Vue!',
        modes_of_transport: {
            "air": {
                "label": "Air",
                "button_label": "Fly",
                "description": 'Get anywhere on the island quickly.',
                "url": '/air_travel_booking',
                "icon": "plane.jpg"
            },
            "driver_provided": {
                "label": "Driver Provided",
                "button_label": "Charter",
                "description": 'Get where you\'re going in comfort.',
                //"url": '/air_travel_booking',
                "url": '/charter_booking',
                "icon": "chauffeur.jpeg"
            },
            "own_transport": {
                "label": "Own Transport (Rental)",
                "button_label": "Rent",
                "description": 'Need to be mobile for a while.',
                "url": '/rental_booking',
                //"url": '/rental_booking',
                "icon": "keys.jpg"
            },
            "doctor_visit": {
                "label": "Doctor Visit",
                "button_label": "Medical Assistance",
                "description": 'Get medical assistance quickly.',
                //"url": '/air_travel_booking',
                "url": '/doctor_visit_booking',
                "icon": "doctor_visit.jpg"
            }
        }
    },
    methods: {
        selectMode: function(mode) {
            window.location.href = BASE_URL + mode.url ;
        }
    }
})