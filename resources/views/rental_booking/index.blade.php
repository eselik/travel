@extends('layouts.master')

@section('title', 'Page Title')


@section('content')

<div clas="row" style="margin-top: 70px;">
    <div class="col-md-7" style="float:left; margin-bottom: 50px;">
        <h3 style="margin-bottom:20px;">Rent a Vehicle</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consequat dapibus leo et mollis. Integer feugiat efficitur dapibus. Aenean at eros vitae enim euismod rhoncus fringilla quis tellus. Cras commodo orci eros, et vestibulum odio porta eu. Nunc a dui nibh. In ac orci non ligula placerat auctor a vel velit. </p>
    </div>
    <div id="mmmodes-of-transport" class=" col-md-4" style="float:right;">
        <form>
            <div class="form-group">
                <label>Pickup</label>
                <select v-model="pickup_point" class="form-control">
                    <option v-for="point in points" v-bind:value="point.value">
                        [[ point.text ]]
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label>Mode Of Transportation</label>
                <select v-model="dropoff_point" class="form-control">
                    <option v-for="mode in modes_of_transport" v-bind:value="mode.value">
                        [[ mode.text ]]
                    </option>
                </select>
            </div>
            <div class="form-group">
                <a href="/rental_booking/show" v-on:click="displayOptions" class="btn btn-primary">View Options</a>
            </div>
        </form>
    </div>
</div>
<!--
<form id="home-screen">
    <div clas="form-group">
        <label>Pickup Location</label>
        <input type="text" name="pickup" class="form-control"/>
    </div>
    <br />
    <div clas="form-group">
        <label>Drop Off</label>
        <input type="text" name="drop_off" class="form-control"/>
    </div>
    <br />
    <div clas="form-group">
        <button class="btn btn-primary" >Find Transportation</button>
    </div>
</form>
-->

<img id="main-image" class="hidden-md-down" src="{{ URL::asset('images/car_vector_1.png') }}" />
@endsection



@section('scripts')
@endsection