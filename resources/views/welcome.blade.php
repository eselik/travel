@extends('layouts.master')

@section('title', 'Page Title')


@section('content')
<!--
<div class="title m-b-md">
    Where are you going today? {{$name}}
</div>
-->
<div clas="row" style="margin-top: 70px;">
    <div class="col-md-5" style="float:left; margin-bottom: 50px;">
        <h3 style="margin-bottom:20px;">TransportJA</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consequat dapibus leo et mollis. Integer feugiat efficitur dapibus. Aenean at eros vitae enim euismod rhoncus fringilla quis tellus. Cras commodo orci eros, et vestibulum odio porta eu. Nunc a dui nibh. In ac orci non ligula placerat auctor a vel velit. Nam metus nunc, congue quis magna quis, tincidunt ultrices leo. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
    </div>
    <div id="mmmodes-of-transport" class=" col-md-6" style="float:right;">
        <div class="row">
            <div class="col-md-6" v-for="mode in modes_of_transport" >
                <div class="card mb-3"  >
                    <img class="card-img-top" v-bind:src="'{{ URL::asset('images/modes_of_transport_icons/') }}' + '/' + mode.icon" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">[[mode.label]]</h5>
                        <p class="card-text">[[mode.description]]</p>
                        <a v-bind:href="mode.url" class="btn btn-primary" v-on:click="selectMode(mode)">[[mode.button_label]]</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--
<form id="home-screen">
    <div clas="form-group">
        <label>Pickup Location</label>
        <input type="text" name="pickup" class="form-control"/>
    </div>
    <br />
    <div clas="form-group">
        <label>Drop Off</label>
        <input type="text" name="drop_off" class="form-control"/>
    </div>
    <br />
    <div clas="form-group">
        <button class="btn btn-primary" >Find Transportation</button>
    </div>
</form>
-->

<!--
<img id="vector-car" class="hidden-md-down" src="{{ URL::asset('images/car_vector_1.png') }}" />
-->
@endsection



@section('scripts')
@endsection