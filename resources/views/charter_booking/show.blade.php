@extends('layouts.master')

@section('title', 'Page Title')


@section('content')

<div clas="row" style="margin-top: 70px;">
    <h3 style="margin-bottom:20px;">Charter Options</h3>
    <hr />
    
    <form class="row">
        <div class="form-group col-sm-7">
            <label>Pickup</label>
            <select v-model="pickup_point" class="form-control">
                <option v-for="point in points" v-bind:value="point.value">
                    [[ point.text ]]
                </option>
            </select>
        </div>
        <div class="form-group col-sm-3">
            <label>Mode Of Transportation</label>
            <select v-model="dropoff_point" class="form-control">
                <option v-for="mode in modes_of_transport" v-bind:value="mode.value">
                    [[ mode.text ]]
                </option>
            </select>
        </div>
        <div class="form-group col-sm-2">
            <label></label>
            <button v-on:click="displayOptions" class="btn btn-primary form-control">Filters Options</button>
        </div>
    </form>
    <hr />
    <div v-for="option in options" class="card">
        <div class="card-header">
            Featured
        </div>
        <div class="card-body">
            <h5 class="card-title">Special title treatment</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Charter</a>
        </div>
    </div>
</div>
@endsection



@section('scripts')
@endsection