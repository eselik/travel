<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer('*', function($view){
            error_log($view->getName());
            $view_name = $view->getName();
            //$view_name = str_replace('.', '-', $view_name);
            $view_name = str_replace('/', '.', $view_name);
            view()->share('view_name', $view_name);
            view()->share('page_script', '/js/'.strtolower($view_name).'.js');
            view()->share('page_stylesheet', '/css/'.strtolower($view_name).'.css');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
