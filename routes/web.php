<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', array("name" => "dwight"));
});

Route::resources([
    'air_travel_booking' => 'AirTravelBookingController',
    'rental_booking' => 'RentalBookingController',
    'charter_booking' => 'CharterBookingController',
    'doctor_visit_booking' => 'DoctorVisitBookingController',
]);